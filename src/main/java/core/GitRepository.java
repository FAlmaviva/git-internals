package core;

import java.io.*;

public class GitRepository {

    private String repoPath;

    public GitRepository(String path){
        repoPath=path;

    }


    public String getHeadRef(){
        File file = new File(repoPath+"/HEAD");
        String ret = "";
        String temp;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((temp = br.readLine()) != null)
                ret+=temp.substring(5);

        }catch(FileNotFoundException e){
            System.out.println("FILE ISSUE");
            e.printStackTrace();
        }catch(IOException e ){
            System.out.println("IOISSUE ISSUE");

            e.printStackTrace();
        }

        return ret;
    }


    public String getRefHash(String s) {
        String hash="";
        String temp;
        try {
            BufferedReader br = new BufferedReader(new FileReader(repoPath+"/refs/heads/master"));
            while ((temp = br.readLine()) != null)
                hash+=temp;

        }catch(FileNotFoundException e){
            System.out.println("FILE ISSUE");
            e.printStackTrace();
        }catch(IOException e ){
            System.out.println("IOISSUE ISSUE");

            e.printStackTrace();
        }
        return hash;

    }
}
