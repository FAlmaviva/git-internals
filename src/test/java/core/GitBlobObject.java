package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {
    String repoPath;
    String hash;

    public GitBlobObject(String rp, String h) {
        repoPath=rp;
        hash = h;

    }

    public String getType() {
        String clusteredHash_high = hash.substring(0,2);
        String clusteredHash_low  = hash.substring(2);
        String ret="";

        try  {
            InflaterInputStream inf = new InflaterInputStream(new FileInputStream(repoPath + "/objects/" + clusteredHash_high + "/" + clusteredHash_low));
            int c =  inf.read();
            while(c!=-1){
                ret+=(char)c;
                c = inf.read();
            }


        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret.substring(0,4);
    }

    public String getContent() {
        String clusteredHash_high = hash.substring(0,2);
        String clusteredHash_low  = hash.substring(2);
        String ret="";

        try  {
            InflaterInputStream inf = new InflaterInputStream(new FileInputStream(repoPath + "/objects/" + clusteredHash_high + "/" + clusteredHash_low));
            int c =  inf.read();
            while(c!=-1){
                ret+=(char)c;
                c = inf.read();
            }


        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret.substring(8);
    }


}
